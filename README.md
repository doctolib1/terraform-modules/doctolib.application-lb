# doctolib.alb

This Terraform module aims to provide an easy way to deploy an Application load balancer on AWS.
This module provision : 

- An ALB
- Security groups
- Target group and associated listeners

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.15 |
| aws | >= 3.39.0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.39.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| alb\_authorized\_ips | The list of authorized IPs. | `list(string)` | `[]` | no |
| alb\_availability\_zones | A list of one or more availability zones for the alb. | `list(string)` | n/a | yes |
| alb\_name | The Application loadbalancer name (must be unique). | `string` | `"doctolib"` | no |
| tags | A map of string containing key value pair to tag created resources. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_alb\_suffix | The AWS Application loadbalancer  arn suffix |
| aws\_alb\_target\_group\_arn | The AWS Application loadbalancer target group arn |
| aws\_alb\_target\_group\_arn\_suffix | The AWS Application loadbalancer target group arn suffix |