data "aws_vpc" "default" {
  default = true
}
resource "aws_default_subnet" "default_az1" {
  for_each = toset(var.alb_availability_zones)
  availability_zone = each.key

  tags = var.tags
}

resource "aws_alb" "alb" {
  name            = var.alb_name
  security_groups = [
    module.ec2_sg.security_group_id,
    module.dev_ssh_sg.security_group_id,
    aws_default_security_group.default.id
  ]
  subnets         = [for subnet in aws_default_subnet.default_az1 : subnet.id]
  tags = var.tags
}

resource "aws_alb_target_group" "group" {
  name     = "${var.alb_name}-target"
  port     = 80
  protocol = "HTTP"

  target_type = "instance"
  vpc_id      = data.aws_vpc.default.id
  stickiness {
    type = "lb_cookie"
  }
  # Alter the destination of the health check to be the login page.
  health_check {
    enabled             = true
    healthy_threshold   = 2
    interval            = 30
    matcher             = "200"
    path                = "/health"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 5
  }
}

resource "aws_alb_listener" "listener_http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.group.arn
    type             = "forward"
  }
}