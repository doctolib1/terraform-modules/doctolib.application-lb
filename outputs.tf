output "aws_alb_target_group_arn" {
  description = "The AWS Application loadbalancer target group arn"
  value       = aws_alb_target_group.group.arn
}

output "aws_alb_target_group_arn_suffix" {
  description = "The AWS Application loadbalancer target group arn suffix"
  value       = aws_alb_target_group.group.arn_suffix
}

output "aws_alb_suffix" {
  description = "The AWS Application loadbalancer  arn suffix"
  value       = aws_alb.alb.arn_suffix
}