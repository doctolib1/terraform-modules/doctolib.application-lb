# Common configurations
variable "alb_name" {
  description = "The Application loadbalancer name (must be unique)."
  type        = string
  default     = "doctolib"
}

variable "alb_availability_zones" {
  description = "A list of one or more availability zones for the alb."
  type        = list(string)
}

variable "tags" {
  description = "A map of string containing key value pair to tag created resources."
  type        = map(string)
  default     = {}
}

# Security configurations
variable "alb_authorized_ips" {
  description = "The list of authorized IPs."
  type        = list(string)
  default     = []
}
