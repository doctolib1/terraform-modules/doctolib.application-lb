resource "aws_default_security_group" "default" {
  vpc_id = data.aws_vpc.default.id

  ingress = [
    {
      protocol  = -1
      description = null
      cidr_blocks = null
      ipv6_cidr_blocks = null
      prefix_list_ids = null
      security_groups = null
      self      = true
      from_port = 0
      to_port   = 0
    }
  ]

  egress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      description = null
      ipv6_cidr_blocks = null
      prefix_list_ids = null
      security_groups = null
      self = null
    }
  ]
}

module "dev_ssh_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "alb_sg"
  description = "Security group for alb_sg"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = [for ip in var.alb_authorized_ips : "${ip}/32"]
  ingress_rules       = ["ssh-tcp"]
}

module "ec2_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "alb_sg"
  description = "Security group for alb_sg"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = [for ip in var.alb_authorized_ips : "${ip}/32"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}