stages:
  - lint

### TERRAFORM LINTER
variables:
  TFLINT_VERSION: "latest"
  TFLINT_RULES: >-
    --enable-rule=terraform_comment_syntax
    --enable-rule=terraform_deprecated_index
    --enable-rule=terraform_deprecated_interpolation
    --enable-rule=terraform_documented_outputs
    --enable-rule=terraform_documented_variables
    --enable-rule=terraform_module_pinned_source
    --enable-rule=terraform_naming_convention
    --enable-rule=terraform_required_providers
    --enable-rule=terraform_required_version
    --enable-rule=terraform_standard_module_structure
    --enable-rule=terraform_typed_variables
    --enable-rule=terraform_unused_declarations
    --enable-rule=terraform_workspace_remote
    --enable-rule=terraform_unused_required_providers

.script_debug: &script_debug
  - |
    # Debug Mode
    if [[ "${DEBUG}" == "true" ]]; then
      echo "Debug enabled"
      set -x
    else
      echo "Debug disabled"
    fi

# Terraform linter for tf files
# Optional variables:
#   TFLINT_VERSION: version of tflint
#   TFLINT_DISABLED: if not null then disabled tflint job
#   TFLINT_OPTS: additional parameters
#   TFLINT_RULES: set of rules to enable (see https://github.com/terraform-linters/tflint/tree/master/docs/rules)
terraform:tflint:
  stage: lint
  image: registry.gitlab.com/doctolib1/terraform-modules/doctolib.auto-scaling-web-app/tflint:latest
  script:
    - *script_debug
    - tflint --version
    - |
      # Installing TFLint GCP Plugin
      curl -sL https://api.github.com/repos/terraform-linters/tflint-ruleset-google/releases/latest |
        jq -r '.assets[] |
        select(.name == "tflint-ruleset-google_linux_amd64.zip").browser_download_url' |
        xargs curl -O -L \
        > /dev/null 2>&1
      mkdir -p ~/.tflint.d/plugins/
      unzip tflint-ruleset-google_linux_amd64.zip -d ~/.tflint.d/plugins/ > /dev/null 2>&1
      echo 'plugin "google" { enabled = true }' > ~/.tflint.hcl
      echo "TFlint GCP plugin installed"
    - |
      # Generating Terraform directory list
      TFLINT_PATHS=$(find . -not -path '*/\.terraform*' -type f -name "*.tf" -exec dirname {} \; |
      sort -u |
      sed 's|^\./||')
      echo -e "Terraform directory to check :\n  ${TFLINT_PATHS}"
      bold_green='\033[1;32m'
      normal='\033[0m'
      RETCODE=0
      local_path="$(pwd)"
      for directory in $TFLINT_PATHS ; do
        # TFlint on found Terraform directory
        echo -e "${bold_green}TFLint on directory \"${directory}\"${normal}"
        cd "${local_path}"
        cd "${directory}"
        # Determine if there are some tflint options
        if [[ -n "${TFLINT_OPTS}" ]]; then
          tflint . -c ~/.tflint.hcl --force ${TFLINT_OPTS} ${TFLINT_RULES}
          echo "TFLint done"
          echo -e "${bold_green}# TFLint Report${normal}"
          tflint . -c ~/.tflint.hcl --format=junit "${TFLINT_OPTS}" ${TFLINT_RULES} \
            > "${local_path}/tflint.$(basename ${directory}).xml" || RETCODE=$((RETCODE+$?))
          echo "Report created"
        else
          tflint . -c ~/.tflint.hcl --force ${TFLINT_RULES}
          echo "TFLint done"
          echo -e "${bold_green}# TFLint Report${normal}"
          tflint . -c ~/.tflint.hcl --format=junit ${TFLINT_RULES} \
            > "${local_path}/tflint.$(basename ${directory}).xml" || RETCODE=$((RETCODE+$?))
          echo "Report created"
        fi
      done
      if [[ $RETCODE -ge 1 ]]; then exit $RETCODE ; fi
  artifacts:
    reports:
      junit: "tflint.*.xml"